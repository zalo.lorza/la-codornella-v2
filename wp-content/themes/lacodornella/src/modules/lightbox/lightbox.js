import Vue from 'vue'
import Bus from '@js/bus'
import select from '@js/select-dom'

export default el => new Vue({
  el,
  data(){
    return {
      isOpen: true,
      isReady: false,
      cells: [],
      includedData: [],
      isLocked: false,
      lastIndex: -1,
      jsInstances: {},
      loadMedia: false
    }
  },
  methods: {
    onSlideshowChange(index){
      if(this.lastIndex === index) return
      this.lastIndex = index
      var $jscomponent = select.find(this.$refs['slideshow-cell'][index], '[lightbox-js-component]')[0]
      if(typeof $jscomponent !== 'undefined'){
        this.jsInstances[index] = render_module($jscomponent, 'lightbox-js-component')
      }

      var instance = this.jsInstances[index];

      if(window.Video 
        && typeof instance !== 'undefined' 
        && typeof instance.type != 'undefined' 
        && instance.type == 'video'){
          window.Video.pauseOthers(instance);
          setTimeout(()=>{window.Video.pauseOthers(instance)}, 600)
      } else {
          window.Video.pauseAll();
          setTimeout(window.Video.pauseAll, 600)
      }
    },
    lockLightbox(){
      this.isLocked = true
    },
    unlockLightbox(){
      this.$nextTick(()=>{
        setTimeout(()=>{
           this.isLocked = false
        }, 1)
      })
    },
    open(data){
      if(this.isLocked) return;
      this.loadMedia = true
      if(window.Video) window.Video.pauseAll();
      this.flkty.select( data.index, false, true )
      this.isOpen = true
      this.$nextTick(()=>{
        this.flkty.resize()
        Bus.$emit('Slideshow.change', data.index)
      })
    },
    close(){
      if(this.isLocked) return;
      this.loadMedia = false
      if(window.Video) window.Video.pauseAll();
      this.isOpen = false
    },
    ready(){
      this.isReady = true
    },
    fetchContent(){
      var index = 0;
      select.all('[data-lightbox]').forEach(el => {
        var data = JSON.parse(el.getAttribute('data-lightbox'))
        data.index = index
        if(!data.type) data.type = 'image'
        if(data.type === 'video') data.src = 'video-'+data.id
        var includedDataIndex = this.includedData.indexOf(data.src);
        if(includedDataIndex === -1){
          this.cells.push(data)
          this.includedData.push(data.src)
        } else {
          data = this.cells[includedDataIndex]
        }
        el.onclick = () => {
          this.open(data)
        }
        index++
      })
    },
    mountInternalComponent($ref, fn, name = null){
      if(name === 'slideshow'){
        this.$nextTick(() => {
          this.flkty = fn($ref, {
              on: {
                ready() {
                  Bus.$emit('Lightbox.close')
                  Bus.$emit('Lightbox.ready')
                },
                change: index => {
                  Bus.$emit('Slideshow.change', index)
                }
              }
            })
        })
      } else {
        this.$nextTick(() => {
          fn(el)
        })
      }
    }
  },
  mounted(){
    Bus.$on('Lightbox.open', this.open)
    Bus.$on('Lightbox.close', this.close)
    Bus.$on('Lightbox.ready', this.ready)
    Bus.$on('Slideshow.dragStart', this.lockLightbox)
    Bus.$on('Slideshow.dragEnd', this.unlockLightbox)
    Bus.$on('Slideshow.change', this.onSlideshowChange)

    this.fetchContent();
    
    this.$nextTick(() => {

      //this.mountJsComponents();

    })
  }
})