<?php

namespace LaCodornella\Managers;


class Theme {

	/** Add timber support, register ACF, register scripts,... */
	public function __construct() {
		
		/**  
	 	* Actions & Filters
		*/
		add_action( 'after_setup_theme', array( $this, 'theme_supports' ) );
		add_filter( 'jpeg_quality', array( $this, 'custom_jpeg_quality'), 10, 2 );
		add_action( 'after_setup_theme', array( $this, 'set_image_sizes' ) );
		add_filter( 'init', array( $this, 'remove_support') );
		add_action( 'wp_print_styles', array( $this, 'deregister_block_styles'), 100 );
	}


	function set_image_sizes(){
		add_image_size( 'blog-thumbnail', 500 );
		add_image_size( 'contained-portrait', 770 );
		add_image_size( 'contained-landscape', 920 );
		add_image_size( 'full-landscape', 1440 );
		add_image_size( 'lightbox', 1920 );
	}

	function deregister_block_styles() {
    wp_dequeue_style( 'wp-block-library' );
	}

	function remove_support(){
		//remove_post_type_support( 'post', 'editor' );
		//remove_post_type_support( 'post', 'thumbnail' );
		register_taxonomy('post_tag', array());
		register_taxonomy('category', array());
	}



	public function theme_supports() {
		// Add default posts and comments RSS feed links to head.
		//add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		
		/*
		 * Enable Menu support
		 *
		 */
		add_theme_support( 'menus' );

		/*
		 * Enable Excerpt
		 *
		 */
		add_post_type_support( 'page', 'excerpt' );
		add_post_type_support( 'post', 'excerpt' );
	}


	/** 
	 * 
	 * Custom Jpeg quality to 100%
	 *
	 */
	public static function custom_jpeg_quality( $quality, $context ) {
		return 100;
	}


}
