<?php

/* Theme namespace */

namespace LaCodornella;

/* Autoload vendors and scripts */

require  __DIR__ . '/Autoload.php';

/* Spin up the app */

global $APP;
$APP = new \Lib\Bootstrap();

/* Custom Theming */

new Managers\Theme();
new Managers\Menus();
new Managers\Gutenberg();
new Managers\RegisterScripts();
new Managers\AdvancedCustomFields();
new Managers\GlobalContext();
new Managers\DisableComments();

/* Render Front Views */

global $VIEW;
$VIEW = new Controller();

// No more code here!
