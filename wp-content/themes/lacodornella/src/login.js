import requireAll from "@js/require-all";

import "@sass/login.scss";

requireAll(require.context("@sass/login", true, /\.scss$/));