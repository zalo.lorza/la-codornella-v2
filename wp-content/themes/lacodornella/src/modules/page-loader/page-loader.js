import Vue from 'vue'

export default el => new Vue({
  el,
  data(){
    return {
      isActive: false,
      animate: false
    }
  },
  methods:{
    show(){
      this.isActive = true
      this.$nextTick(() => {
        setTimeout(()=>{
          this.animate = true
        }, 1)
      })
    }
  },
  mounted(){
    window.onbeforeunload = () => { this.show() }
  }
})