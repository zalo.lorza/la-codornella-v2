<?php

if($context['post']->thumbnail->post_mime_type == 'image/svg+xml'){
  return file_get_contents($context['post']->thumbnail->guid, 
        false, 
        stream_context_create(array(
          "ssl"=>array(
            "verify_peer"=>false,
            "verify_peer_name"=>false,
          )
        )));
}