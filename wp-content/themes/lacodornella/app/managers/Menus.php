<?php

namespace LaCodornella\Managers;

/**
* Menus Class
*/

class Menus extends \Lib\Menus {


    function __construct() {

        $menus = array(
					'main' => __( 'Main Menu' ),
                    'top' => __( 'Top' ),
                    'footer' => __( 'Footer' )
        );
        
        parent::__construct($menus);

        return $this;

    }

}
