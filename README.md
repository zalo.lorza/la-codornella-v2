# La Codornella Wordpress Theme

## Project setup
```
npm install
```

### Compiles for development
```
npm run dev
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
