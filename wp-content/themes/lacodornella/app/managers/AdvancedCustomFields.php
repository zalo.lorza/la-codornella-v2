<?php

namespace LaCodornella\Managers;

/**
 * Register ACF fields, Options Page,...
 */

class AdvancedCustomFields extends \Lib\AdvancedCustomFields {

    
    public function __construct() {

        parent::__construct();

        $this->register_fields_php();
        $this->register_options_pages();

        add_filter( 'acf/fields/wysiwyg/toolbars' , array($this,'set_toolbars')  );
        add_filter('acf/settings/load_json',  array($this,'json_load_point'));
        add_filter('acf/settings/save_json', array($this,'json_save_point'));

    }



    /**
    *    bidirectional_acf_update_value
    */

    function bidirectional_acf_update_value( $value, $post_id, $field  ) {

        // vars
        $field_name = $field['name'];
        $field_key = $field['key'];
        $global_name = 'is_updating_' . $field_name;
        
        
        // bail early if this filter was triggered from the update_field() function called within the loop below
        // - this prevents an inifinte loop
        if( !empty($GLOBALS[ $global_name ]) ) return $value;
        
        
        // set global variable to avoid inifite loop
        // - could also remove_filter() then add_filter() again, but this is simpler
        $GLOBALS[ $global_name ] = 1;
        
        
        // loop over selected posts and add this $post_id
        if( is_array($value) ) {
        
            foreach( $value as $post_id2 ) {

                if(defined('ICL_LANGUAGE_CODE') && apply_filters( 'wpml_post_language_details', NULL, $post_id )['locale'] != apply_filters( 'wpml_post_language_details', NULL, $post_id2 )['locale']){
                    continue;
                }
                
                // load existing related posts
                $value2 = get_field($field_name, $post_id2, false);
                
                
                // allow for selected posts to not contain a value
                if( empty($value2) ) {
                    
                    $value2 = array();
                    
                }
                
                
                // bail early if the current $post_id is already found in selected post's $value2
                if( in_array($post_id, $value2) ) continue;
                
                
                // append the current $post_id to the selected post's 'related_posts' value
                $value2[] = $post_id;
                
                
                // update the selected post's value (use field's key for performance)
                update_field($field_key, $value2, $post_id2);
                
            }
        
        }
        
        
        // find posts which have been removed
        $old_value = get_field($field_name, $post_id, false);
        
        if( is_array($old_value) ) {
            
            foreach( $old_value as $post_id2 ) {
                
                // bail early if this value has not been removed
                if( is_array($value) && in_array($post_id2, $value) ) continue;
                
                
                // load existing related posts
                $value2 = get_field($field_name, $post_id2, false);
                
                
                // bail early if no value
                if( empty($value2) ) continue;
                
                
                // find the position of $post_id within $value2 so we can remove it
                $pos = array_search($post_id, $value2);
                
                
                // remove
                unset( $value2[ $pos] );
                
                
                // update the un-selected post's value (use field's key for performance)
                update_field($field_key, $value2, $post_id2);
                
            }
            
        }
        
        
        // reset global varibale to allow this filter to function as per normal
        $GLOBALS[ $global_name ] = 0;
        
        
        // return
        return $value;
        
    }


    /**
    *   Register all ACF fields via PHP
    */

    private static function register_fields_php() {

        if( !function_exists('acf_add_local_field_group') ) return;

        foreach (glob(dirname(__DIR__)."/acf-fields/*.php") as $filename){
            include_once $filename;
        }
        
    }

     /**
    *   Register ACF fields via Json
    */
    public static function register_fields_json($filename) {

        if( !function_exists('acf_add_local_field_group') ) return;

        $fields = json_decode(file_get_contents($filename), true);
        self::register_field($fields);
        
    }

    private static function register_field($fields) {
      
        if(!isset($fields['key']) || !$fields['key']){
            foreach($fields as $field){
                self::register_field($field);
            }
        } else {
            acf_add_local_field_group($fields);
        }
    }


    /**
    *   Register all ACF Options Pages
    */

    private function register_options_pages() {

        if( function_exists('acf_add_options_page') ) {

            /*acf_add_options_page(array(
                'page_title' 	=> 'Theme options',
                'menu_title'	=> 'Theme options',
                'menu_slug' 	=> 'theme-options',
                'capability'	=> 'edit_posts',
                'redirect'		=> false
            ));*/
        
        }
    }



    /**
    *   Set TinyMCE Toolbars for ACF
    */

    public function set_toolbars($toolbars) {

      	// Uncomment to view format of $toolbars
        /*
        echo '< pre >';
            print_r($toolbars);
        echo '< /pre >';
        die;
        */

        // Add a new toolbar called "Very Simple"
        // - this toolbar has only 1 row of buttons
        /*$toolbars['Only link' ] = array();
        $toolbars['Only link' ][1] = array('link');


        $toolbars['Only bold' ] = array();
        $toolbars['Only bold' ][1] = array('bold');

        $toolbars['Bold and links' ] = array();
        $toolbars['Bold and links' ][1] = array('bold','link', 'italic','|','undo','redo');


        $toolbars['List and headlines' ] = array();
        $toolbars['List and headlines' ][1] = array('link','bold','bullist','|','undo','redo');*/

        $toolbars['Very Simple' ] = array();
        $toolbars['Very Simple' ][1] = array('link');

        

        // Edit the "Full" toolbar and remove 'code'
        // - delet from array code from http://stackoverflow.com/questions/7225070/php-array-delete-by-value-not-key
 
        if( ($key = array_search('code' , $toolbars['Full' ][2])) !== false )
        {
            unset( $toolbars['Full' ][2][$key] );
        }

        // remove the 'Basic' toolbar completely
        unset( $toolbars['Basic' ] );

        // return $toolbars - IMPORTANT!
        return $toolbars;
        
    }


    // JSON

    public static function json_load_point( $paths ) {
        
        // remove original path (optional)
        unset($paths[0]);
        
        // append path
        $paths[] = get_stylesheet_directory() . '/app/acf-fields';
        
        // return
        return $paths;
        
    }

 
    public static function json_save_point( $path ) {
        
        // update path
        $path = get_stylesheet_directory() . '/app/acf-fields';
        
        
        // return
        return $path;
        
    }

}
