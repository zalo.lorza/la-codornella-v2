'use strict';

export default function requireAll(r) {
    r.keys().forEach(r);
}