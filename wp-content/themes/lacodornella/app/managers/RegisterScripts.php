<?php

namespace LaCodornella\Managers;

/**
 * Register CSS and JS scripts for theme
 */

class RegisterScripts {

    public function __construct() {


        /**
        * Set scripts root
        */

        if ( NODE_ENV == 'development'  ) {
            define('SCRIPTS_ROOT','http://localhost:8084');
        } else {
            define('SCRIPTS_ROOT',get_template_directory_uri());
        }


        /**
        * Deregister scripts
        */

        add_action( 'wp_footer', array($this, 'deregister_scripts') );
        remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); 
        remove_action( 'admin_print_scripts', 'print_emoji_detection_script' ); 
        remove_action( 'wp_print_styles', 'print_emoji_styles' ); 
        remove_action( 'admin_print_styles', 'print_emoji_styles' );


        /**
        * Register theme scripts
        */

        add_action( 'wp_enqueue_scripts', array($this,'register_vendor_script'));
        add_action( 'wp_enqueue_scripts', array($this,'register_theme_scripts'));
        add_action( 'wp_enqueue_scripts', array($this,'expose_js_vars'));


        /**
        * Register admin scripts
        */

        add_action('admin_enqueue_scripts', array($this,'register_vendor_script'));
        add_action('admin_enqueue_scripts', array($this,'register_admin_scripts'));
        //add_action('enqueue_block_editor_assets', array($this,'register_gutenberg_scripts'));
        add_action( 'login_enqueue_scripts', array($this,'login_styles') );

      

    }

    function login_styles() {
        wp_enqueue_style( 'custom-login',  SCRIPTS_ROOT . '/dist/login.'.SCRIPTS_HASH.'.css', array(), null);
    }


    /**
    * Expose JS Vars
    */

    public function expose_js_vars(){

        wp_localize_script( 'theme-js', 'NODE_ENV', NODE_ENV);

        wp_localize_script( 'theme-js', 'user', array(
            'is_user_logged_in' => is_user_logged_in()
        ));
    }


    /**
    * Deregister WP scripts
    */

    public function deregister_scripts(){

        wp_deregister_script( 'wp-embed' );

        if (!is_admin()){
            wp_deregister_script('jquery');
        }

    }


    /**
    * Vendor scripts
    */

    public function register_vendor_script(){
        wp_enqueue_script( 'theme-vendor-static', SCRIPTS_ROOT . '/dist/static.vendor.'.SCRIPTS_HASH.'.js', array(), null, true );
    }



    /**
    * Theme scripts
    */

    public function register_theme_scripts(){

        /**
        * Main JS and CSS
        */

        wp_enqueue_script( 'theme-js', SCRIPTS_ROOT . '/dist/build.'.SCRIPTS_HASH.'.js', array(), null, true );
        wp_enqueue_style( 'theme-css',  SCRIPTS_ROOT . '/dist/build.'.SCRIPTS_HASH.'.css', array(), null);

        /**
        * Expose JS globals
        */

        $base_url  = esc_url_raw( home_url() );
        $base_path = rtrim( parse_url( $base_url, PHP_URL_PATH ), '/' );
    
        wp_localize_script( 'theme-js', 'theme', array(
            'root'      => esc_url_raw( rest_url() ),
            'base_url'  => $base_url,
            'base_path' => $base_path ? $base_path . '/' : '/',
            'site_url'  => site_url(),
            'nonce'     => wp_create_nonce( 'wp_rest' ),
            'site_name' => get_bloginfo( 'name' ),
            'front_page_id' => get_option( 'page_on_front' ),
            'is_front_page_id' =>  is_front_page(),
            'stylesheet_directory_uri' => get_stylesheet_directory_uri()
        ) );
       
        wp_localize_script( 'theme-js', 'ajaxurl', admin_url( 'admin-ajax.php' ));


        if(defined('ICL_LANGUAGE_CODE')){
            wp_localize_script( 'theme-js', 'LANG', ICL_LANGUAGE_CODE );
        }
    
    }


    /**
    * Admin scripts
    */

    public function register_admin_scripts(){

        wp_enqueue_style( 'custom-admin-css',   SCRIPTS_ROOT . '/dist/admin.'.SCRIPTS_HASH.'.css', array(), null );
        wp_enqueue_script( 'custom-admin-js', SCRIPTS_ROOT . '/dist/admin.'.SCRIPTS_HASH.'.js', array(), null, true );
    }


    /**
    * Gutenberg block scripts
    */

    public function register_gutenberg_scripts(){

        wp_enqueue_style(
            'custom_gutenberg_block_styles',
            SCRIPTS_ROOT . '/dist/gutenberg.'.SCRIPTS_HASH.'.css',
            null, 
            null
        );

    }



}
